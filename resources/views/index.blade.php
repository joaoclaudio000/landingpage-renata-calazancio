<!DOCTYPE html>
<!--  This site was created in Webflow. http://www.webflow.com  -->
<!--  Last Published: Tue Oct 03 2017 18:35:05 GMT+0000 (UTC)  -->
<html data-wf-page="59ca508efcfbf3000197c085" data-wf-site="59ca508dfcfbf3000197c080">
<head>
  <meta charset="utf-8">
  <title>Renata Calazancio</title>
  <meta content="width=device-width, initial-scale=1" name="viewport">
  <meta content="Webflow" name="generator">
  <link href="css/normalize.css" rel="stylesheet" type="text/css">
  <link href="css/webflow.css" rel="stylesheet" type="text/css">
  <link href="css/renata-calazancio.webflow.css" rel="stylesheet" type="text/css">
  <script src="https://ajax.googleapis.com/ajax/libs/webfont/1.4.7/webfont.js" type="text/javascript"></script>
  <script type="text/javascript">WebFont.load({  google: {    families: ["Lato:100,100italic,300,300italic,400,400italic,700,700italic,900,900italic"]  }});</script>
  <!-- [if lt IE 9]><script src="https://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv.min.js" type="text/javascript"></script><![endif] -->
  <script type="text/javascript">!function(o,c){var n=c.documentElement,t=" w-mod-";n.className+=t+"js",("ontouchstart"in o||o.DocumentTouch&&c instanceof DocumentTouch)&&(n.className+=t+"touch")}(window,document);</script>
  <link href="https://daks2k3a4ib2z.cloudfront.net/img/favicon.ico" rel="shortcut icon" type="image/x-icon">
  <link href="https://daks2k3a4ib2z.cloudfront.net/img/webclip.png" rel="apple-touch-icon">
  <script src="https://use.fontawesome.com/47b08918bc.js"></script>
</head>
<body class="body">
  <div class="overflow-hidden">
    <div data-collapse="medium" data-animation="over-left" data-duration="200" class="navbar w-nav">
      <div class="container">
        <nav role="navigation" class="navbar-menu w-nav-menu"><a href="#header" data-ix="hoverlink" class="navlink w-nav-link">Início</a><a href="#servicos" data-ix="hoverlink" class="navlink w-nav-link">Serviços</a><a href="#contato" data-ix="hoverlink" class="navlink w-nav-link">Local</a><a href="#contato" data-ix="hoverlink" class="navlink w-nav-link">Contato</a></nav><a href="#" class="brand w-nav-brand"><img src="images/header--logo-green.svg"></a>
        <div class="button-menu w-nav-button"><img src="images/hamburguer.svg" class="image"></div>
      </div>
    </div>
    <div id="header" class="header">
      <div class="microcopy">
        <h1 data-ix="fadedown" class="microcopy-tittle">Cuidando de você</h1>
        <p data-ix="fadedown-2" class="microcopy-text">Somos uma empresa com mais de 20 anos de experiência focada no bem estar de nossas clientes</p><a href="#contato" data-ix="fadedown-3" class="button w-button">Fale Conosco</a></div>
      <div class="header--img-woman"><img src="images/background-woman.png" data-ix="faderigthimg" class="image-2"></div>
      <a href="#servicos" data-ix="scroller" class="scroller w-inline-block">
        <div class="scroller-inner"></div>
      </a>
    </div>
    <div id="servicos" class="section">
      <div class="center section-header">
        <h1 data-ix="fadedownonscroll" class="section-title">Serviços</h1>
      </div>
      <div class="container flex">
        <div class="flex-container">
          <div data-ix="fadeleftimg" class="bg colum services-box"></div>
          <div class="colum services-box">
            <div class="width-limit">
              <h3 data-ix="fadedownonscroll" class="service-item--title">Depilação com cera</h3>
              <div data-ix="lineinscroll" class="linha"></div>
            </div>
            <p data-ix="fadedownonscroll" class="paragraph">Pele lisa, macia e livre de pelos por muito mais tempo. Além dos pelos demorarem para crescer, usamos cera com componentes 100% naturais, livre de essências ou corantes, hipoalergênica, ideal para peles sensíveis. Este método diminui progressivamente o nascimento do pelo. </p>
          </div>
        </div>
        <div class="flex-container reverse">
          <div class="colum services-box">
            <div class="width-limit">
              <h3 data-ix="fadedownonscroll" class="service-item--title">Depilação com linha</h3>
              <div data-ix="lineinscroll" class="linha"></div>
            </div>
            <p data-ix="fadedownonscroll" class="paragraph">Depilação com linha é uma técnica de depilação temporária, que consiste em utilizar apenas um fio 100% de algodão. Esse fio é torcido e enrolado com as duas mãos deslizando sobre a pele, entrelaçando o pelo com o fio enrolado, retirando-o a partir do folículo, resultando numa área totalmente depilada, muito limpa e precisa.</p>
          </div>
          <div data-ix="faderigthimg" class="bg2 colum services-box"></div>
          <div class="flex-container">
            <div data-ix="fadeleftimg" class="bg3 colum services-box"></div>
            <div class="colum services-box">
              <div class="width-limit">
                <h3 data-ix="fadedownonscroll" class="service-item--title">Banho de lua</h3>
                <div data-ix="lineinscroll" class="linha"></div>
              </div>
              <p data-ix="fadedownonscroll" class="paragraph">O banho de lua é uma técnica que consiste em clarear os pelos do corpo através de produtos químicos, seja para estética, para disfarça-los, como para destacar o bronzeado e ficar com a pele macia e hidratada.</p>
            </div>
            <div class="flex-container reverse">
              <div class="colum services-box">
                <div class="width-limit">
                  <h3 data-ix="fadedownonscroll" class="service-item--title">Cílios fio a fio</h3>
                  <div data-ix="lineinscroll" class="linha"></div>
                </div>
                <p data-ix="fadedownonscroll" class="paragraph">Para quem deseja um olhar mais expressivo e sensual, essa técnica consiste na aplicação de fios sintéticos que serão colados aos outros fios naturais dos cílios. Esse procedimento é indicado para dar volume e alongar os cílios. Dura até 30 dias, sendo necessário realizar manutenção semanalmente ou a cada 10 dias.</p>
              </div>
              <div data-ix="faderigthimg" class="bg4 colum services-box"></div>
              <div class="flex-container">
                <div data-ix="fadeleftimg" class="bg5 colum services-box"></div>
                <div class="colum services-box">
                  <div class="width-limit">
                    <h3 data-ix="fadedownonscroll" class="service-item--title">Sobrancelha</h3>
                    <div data-ix="lineinscroll" class="linha"></div>
                  </div>
                  <p data-ix="fadedownonscroll" class="paragraph">Aplicado de forma personalizada, com pinça de alta precisão, por uma designer especializada, levando em consideração diversos aspectos do rosto, tais como: linhas de expressão, simetria facial, quantidade, cor e tipo dos fios. Nossa técnica permite, realçando sua beleza natural</p>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div id="testimonials" class="odd section">
      <div class="center margin-bottom section-header">
        <h1 data-ix="fadedownonscroll" class="section-title">+20 anos de experiência</h1>
      </div>
      <div class="container">
        <div class="flex-container">
          <div data-ix="depoimento1" class="bg-depoimento colum testimonial-box"><img src="images/quote.png">
            <p class="quote">&quot;Sou cliente fiel da Renata calazancio depilação há anos. O profissionalismo, pontualidade e o respeito pelas clientes sempre me transmitiram segurança e me  fidelizaram durante esses quase 20 anos. Hoje, tenho o prazer de estender essa satisfação, trazendo a minha filha aos cuidados da mesma profissional e equipe que cuidou e ainda cuida de mim. Feliz por fazer parte dessa família. Flaviana</p>
            <div class="person-name">- flaviananubia</div>
          </div>
          <div data-ix="depoimento-2" class="bg-depoimento colum testimonial-box"><img src="images/quote.png">
            <p class="quote">&quot;Comecei a depilar com a Renata já tem alguns anos. Depilar sempre foi difícil pra mim. Sinto mta dor e desconforto. Mas quando conheci a Renata tudo ficou mais tranquilo. Com a técnica e carinho que ela me depila, superei meu trauma. Não troco a Renata por ninguém nesse mundo. Meu marido vive mandando eu depilar em outro lugar por conta da distância da Renata Calazancio da minha casa. Mas não tem distância nenhuma que pague a tranquilidade que tenho quando depilo com a Renata. Já temos uma relação de amizade! Ela me depilou nos momentos mais importante da minha vida (meu casamento e antes do meu parto). Sou muito feliz por conhecer o trabalho dessa empresa que eu gosto muito.&quot;</p>
            <div class="person-name">- Amanda</div>
          </div>
          <div data-ix="depoimento-3" class="bg-depoimento colum testimonial-box"><img src="images/quote.png">
            <p class="quote">&quot;Depilo com a Renata há quase 20 anos, e sempre falo, é o dinheiro mais bem gasto. Pago com satisfação. Atendimento impecável. Por isso a fidelidade de tantos anos.&quot;</p>
            <div class="person-name">- Binuí</div>
          </div>
        </div>
      </div>
    </div>
    <div class="cta section white">
      <div class="center section-header">
        <h1 data-ix="fadedownonscroll" class="section-title white">Fale conosco em um clique</h1>
        <p class="section-header--text">Agora você pode tirar dúvidas, fazer agendamentos, ou até mesmo enviar seu depoimento diretamente no whatsapp.</p>
        <a href="https://api.whatsapp.com/send?phone=5561982257872&amp;text=Teste%20de%20integração%20xpbox" data-ix="hoverlink" target="_blank" class="button-white-whatsapp w-inline-block">
          <div class="font-awesome"></div>
          <div class="paragraph-whatsapp">fale conosco pelo <strong>whatsapp</strong></div>
        </a>
      </div>
    </div>
    <div id="contato" class="section">
      <div class="center margin-bottom section-header">
        <h1 data-ix="fadedownonscroll" class="section-title">Fale Conosco</h1>
      </div>
      <div class="container">
        <div class="flex-container">
          <div id="form-contact" class="colum contact-box odd">
            <div class="form-block w-form">
              <form id="email-form" name="email-form" data-name="Email Form" class="form"><label for="name" data-ix="fadeleftimg" class="field-label">Name:</label><input type="text" class="input w-input" maxlength="256" name="name" data-name="Name" data-ix="fadedownonscroll" placeholder="Nome" id="name" required=""><label for="email-2" data-ix="fadeleftimg" class="field-label">Email:</label><input type="email" class="input w-input" maxlength="256" name="email" data-name="email" data-ix="fadedownonscroll" placeholder="Seu email mais usado" id="email-2" required=""><label for="phone" data-ix="fadeleftimg" class="field-label">Telefone:</label><input type="text" class="input w-input" maxlength="256" name="phone" data-name="phone" data-ix="fadedownonscroll" placeholder="Seu telefone" id="phone" required=""><input type="submit" value="Enviar menssagem" data-wait="Enviando..." data-ix="fadedownonscroll" class="button-line fullwidth w-button"></form>
              <div class="success-message w-form-done">
                <div class="text-block">Obrigado por entrar em contato, logo retornaremos sua menssagem!</div>
              </div>
              <div class="error-message w-form-fail">
                <div class="text-block-2">Algo não está certo, por favor confira os campos</div>
              </div>
            </div>
          </div>
          <div class="colum contact-box odd">
            <a href="#" class="link-maps w-inline-block">
              <p data-ix="faderigthimg" class="paragraph-txt"><strong>Endereço</strong><br>QNB 8 Lote 02 Loja 01, Taguatinga Norte,<br>Brasília - DF, 72115-080</p>
            </a><a href="https://www.google.com.br/maps/place/Centro+de+Depila%C3%A7%C3%A3o+Ranata+Calazancio/@-15.826929,-48.0649854,17z/data=!3m1!4b1!4m5!3m4!1s0x935a32de7d5a23c1:0x819fb0dc5f7e2fde!8m2!3d-15.8269342!4d-48.0627967" data-ix="faderigthimg" target="_blank" class="link">Abrir no google maps</a>
            <p data-ix="faderigthimg" class="paragraph-txt"><strong>Telefone</strong><br>(61) 3561-0244</p>
            <div class="social-icon-container">
              <a href="https://www.facebook.com/renatacalazanciodepilacao" data-ix="fadedownonscroll" target="_blank" class="link-block---social w-inline-block">
                <div class="icon social-icon"></div>
              </a>
              <a href="https://www.instagram.com/renatacalazanciodepilacao/" data-ix="fadedownonscroll" target="_blank" class="link-block---social w-inline-block">
                <div class="icon social-icon"></div>
              </a>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="footer section">
      <div id="form-contact" class="flex-container spacebetween">
        <div class="colum">
          <div class="footer-text-block">Renata Calazancio © 2017</div>
        </div>
        <div class="colum">
          <div class="footer-text-block">Make with <em class="icon"></em> by <a href="http://xpbox.com.br" target="_blank" class="xpbox-link">Xpbox</a></div>
        </div>
      </div>
    </div>
  </div>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js" type="text/javascript"></script>
  <script src="js/webflow.js" type="text/javascript"></script>
  <!-- [if lte IE 9]><script src="https://cdnjs.cloudflare.com/ajax/libs/placeholders/3.0.2/placeholders.min.js"></script><![endif] -->
</body>
</html>